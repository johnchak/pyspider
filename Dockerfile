FROM python:3.6

# PhantomJS
WORKDIR /opt/phantomjs
COPY phantomjs.tar.bz2 .
RUN tar xavf phantomjs.tar.bz2 --strip-components 1 \
    && ln -s /opt/phantomjs/bin/phantomjs /usr/local/bin/phantomjs \
    && rm phantomjs.tar.bz2

# add all repo
COPY . /opt/pyspider

# run test
WORKDIR /opt/pyspider
RUN pip install -e .[all]

VOLUME ["/opt/pyspider"]

WORKDIR /home/service

EXPOSE 5000 23333 24444 25555
